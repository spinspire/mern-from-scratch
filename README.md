# MERN from scratch

This repository contains a project with several branches. Each branch shows the evolution of the project from a basic ReactJS app to a complex MERN (Mongo-Express-React-NodeJS) app.

# How to use

Start with branch named episode-1 and make your way to later episodes. This code is accompanied by videos on our YouTube channel at https://www.youtube.com/spinspire. Look for a playlist named "MERN from scratch" there.

But please keep in mind that the branch shows state of the code at the END of the episode, not beginning. So you're better off starting from the code from previous episode when starting to follow an episode.

# Episode 1 - Basic ReactJS app

- How to create a package.json
- Which dependencies to add to package.json and why
- Writing the smallest and simplest `webpack.config.js` and know what everything does
- Creating the simplest ReactJS app
- Running the `webpack-dev-server` that allows live coding and hot reloading
