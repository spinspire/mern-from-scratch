const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    output: {
        path: __dirname + '/build',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/, // apply to .js and .jsx files
                exclude: /(node_modules|bower_components)/, // don't apply to these
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env', // targets various browsers
                            '@babel/preset-react', // makes it possible to use JSX and ES6 inside JS files
                        ],
                    }
                }
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
        })
    ],
};