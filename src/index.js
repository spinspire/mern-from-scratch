import React from "react"
import ReactDOM from "react-dom"

function App() {
    return (
        <div>
            <h4>App Component</h4>
            <p>App component more stuff ...</p>
        </div>
    )
}
ReactDOM.render(<App/>, document.getElementById('app-root'))